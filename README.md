# Crypto Search Website #

[evildeedz.gitlab.io/crypto-search](https://evildeedz.gitlab.io/crypto-search/)

_This personal project was made to showcase the use of **React Hooks**, as well as use of **Third Party API's** and rendering them to page. This website takes list of **Top 100 Crypto Coins by their Market Cap**. Responsive on all devices._

<img src="https://i.imgur.com/uUjoyv6.png">


## Search Box ##

  The search box is a simple input field. On value change (or when you type anyhting in it) it saves its value to a state variable using the **useState** hook. Each time a state value is changed the page re-renders. This makes it easier for shortlisting values, as explaining below.


##  Rendering API ##

 We use [CoinGecko's API](https://www.coingecko.com/en/api/documentation) to get the list of the coins using the **useEffect** hook on page render. We then use **.filter()* to only get the coins in need. This means that if we have any value typed in the search box, it will only render the coins which have the characters of the search valaue in its name or symbol. For example if we type _Pol_ in the search box, we get _Polkadot_ and _Polygon_ cryptos shortlisted. We can also type MATIC in the searfch box and still get Polygon as we search through symbols as well. All coins are rendered if the value of the search box is null (or "").

**Searching Using Name**

<img src="https://i.imgur.com/giVvwUa.png">

**Searching Using Symbol**

<img src="https://i.imgur.com/PrAVNhr.png">
