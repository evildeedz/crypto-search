import React, { useEffect, useState } from "react";
import "./Homepage.css";
import Axios from "axios";
import Crypto from "../Components/Crypto";

function Homepage() {
  const [cryptos, setCryptos] = useState([]);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setInterval(() => {
      refresh();
    }, 2000);
  }, []);

  function refresh() {
    // Getting List of Cryptos
    Axios.get("https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd")
      .then((response) => {
        setCryptos(response.data);
        setLoading(false);
      })
      .catch((err) => {
        alert(err);
      });
  }

  return (
    <div id="homepage-div">
      <div className="title">
        <h1 className="py-4 text-light">Crypto Search</h1>
      </div>
      <div className="main py-5">
        <input type="text" placeholder="Search.." className="search px-2 my-4" value={search} onChange={(event) => setSearch(event.target.value)}></input>
        <div className="crypto-list">
          {loading && <h3 className="loading py-2">Loading..</h3>}
          {cryptos
            .filter((crypto) => {
              if (search === "") {
                return crypto;
              } else if (crypto.name.toLowerCase().includes(search.toLowerCase())) {
                return crypto;
              } else if (crypto.symbol.toLowerCase().includes(search.toLowerCase())) {
                return crypto;
              }
            })
            .map((crypto) => {
              return <Crypto key={crypto.id} symbol={crypto.symbol} image={crypto.image} name={crypto.name} currentPrice={crypto.current_price} marketCap={crypto.market_cap} priceChange={crypto.price_change_percentage_24h} />;
            })}
        </div>
      </div>
    </div>
  );
}

export default Homepage;
