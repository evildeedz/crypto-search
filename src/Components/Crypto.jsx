import React from "react";
import "./Crypto.css";

function Crypto(props) {
  const internationalNumberFormat = new Intl.NumberFormat("en-US");

  return (
    <div id="crypto-div" className="mt-5">
      <center>
        <div className="container-fluid">
          <div className="row">
            <div className="img-div col-lg-2">
              <img className="crypto-img" src={props.image} alt="test" />
            </div>
            <div className="col-lg-2">
              <h5>{props.name}</h5>
            </div>
            <div className="col-lg-2">
              <h5>{props.symbol.toUpperCase()}</h5>
            </div>
            <div className="col-lg-2">
              <h5>{"$" + internationalNumberFormat.format(props.currentPrice)}</h5>
            </div>
            <div className="col-lg-2">{!props.priceChange.toString().includes("-") ? <h5 className="text-success">{props.priceChange + "%"}</h5> : <h5 className="text-danger">{props.priceChange + "%"}</h5>}</div>
            <div className="col-lg-2">
              <h5>{"$" + internationalNumberFormat.format(props.marketCap)}</h5>
            </div>
          </div>
        </div>
      </center>
      <hr />
    </div>
  );
}

export default Crypto;
